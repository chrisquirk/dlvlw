\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}

\title{Discriminative Latent Topics for Machine Translation}
\author{Chris Quirk, Hal Daum\'e III, Ann Clifton, others from DAMT team?}

\begin{document}
\newcommand{\pd}[2]{\frac{\partial}{\partial #1}\left[ #2 \right]}
\newcommand{\E}[2]{\mathbb{E}_{#1} \left[ #2 \right]}

\maketitle

\section{Introduction}

Our goal here is to learn latent sumdomains (topics) from parallel data.
Traditional topic models are monolingual: they find a mixture of unigram distributions that optimizes the likelihood of some monolingual document set.
There are a set of unigram distributions, one for each of the latent topics.
Each document is assigned its own mixture distribution, and each word within that document has some assignment or distribution to those topics.
These topics have been successfully used for learning more specific lexical translation distributions (\emph{cite Eidelman!}) for machine translation systems.

However, the topics only look at one side of the parallel data.
Intuitively it would make sense to learn topics that leverage both languages.
Several approaches have been suggested for polylingual topic distributions: UMass Amherst, Microsoft, UMD.
These approaches generally try to model the joint likelihood of both documents.

For translation, though, it would make more sense to model the \emph{conditional} likelihood of the target language given the source.
Furthermore, there are several deficiencies of the generative topic models that we would like to address.

A first limitation is that each word gets an equal voice in selecting the topic distribution of the document.
In a conventional LDA topic model, the  probability of a document is $$P(\theta| \alpha) \prod_i P(z_i | \theta) P(w_i | z_i).$$
The posterior distribution over topics looks like a na\"ive Bayes model given the words: each word gets equal weight in selecting topics.
This is unfortunate.
Many common words (such as ``\emph{the}'' or ``\emph{is}'') have no strong preference amongst topics.
They translate in the same way regardless of topic.
Other words may be strong indicators of a particular topic (\emph{provide example}).

A second limitation is that each topic learns a totally independent distribution.
In practice, some words translate in different ways depending on the topic; others are more consistent across varying contexts.
We would like a model that addresses this with sharing.

\section{Discriminative latent variable topics}

Our idea here is to replace the generative model with a discriminative one that optimizes likelihood directly.
First, we predict the probability of each topic using a log-linear model with features from the whole source document.
This allows some words to vote strongly for particular topics, and others to quietly vacillate without influencing the distribution substantially.

Second, we replace the translation distribution with another log-linear distribution.
We assume that there are $2^B$ topics for some value $B$, and that they live in a simple hierarchy consisting of a binary tree.
Say we have $B=2$, so there are four topics.
Then in addition to the leaf topics $0, 1, 2,$ and $3$, we add three ``super-topics'': $\{0,1\}, \{2,3\},$ and $\{0,1,2,3\}$.
When extracting features for, say, topic $2$, we also emit features for the super-topics $\{2,3\}$ and $\{0,1,2,3\}$.
This allows the parameter estimation procedure to set parameters at the appropriate level of the hierarchy.
Words that do not depend on the topic assignment may have most of their parameters set of the root of the topic tree.
Other words that are influenced by context may learn parameters at lower levels.

We devote the remainder of this section to the formal description of the model and estimation of its parameters.

\subsection{Notation}

\begin{itemize}
\item $\mathbf{\Sigma}, \mathbf{T}$: source and target language vocab
\item $S$: Source language document
\item $T$: Target language document
\item $s,t$: source and target language words
\item $K$: number of topics
\item $Z = \{z_1, \ldots, z_k\}$: topics
\item $F : 2^{\mathbf{\Sigma}} \times Z \rightarrow R^m$: topic feature function
\item $G : S \times Z \times T \rightarrow R^n$: translation feature function
\item $\theta \in R^m, \phi \in R^n$: parameter vectors for topics and translations, respectively
\end{itemize}

\subsection{Model}

We aim to model the conditional likelihood of a target document given a source document, using a mixture of latent topics:
$$ P(T|S) = \sum_{z\in Z} \left( P(z|S) \prod_{(s,t)\in(S,T)} P(t|s,z) \right)$$
The topic distribution is predicted based on features of the whole source document:
$$P(z|S) \propto \exp(\theta \cdot F(S, z))$$
Each translation is predicted based only on the source word and a given topic likelihood:
$$P(t|s,z) \propto \exp(\phi \cdot G(s, z, t))$$
So the likelihood, expanded out, is as follows:
$$ P(T|S,\theta,\phi) = \sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)$$
Here is the log likelihood of a single target document
Note that the sum over mixture components prevents the log from further advances, unlike standard logistic regression models.
$$ \log P(T|S,\theta,\phi) = \log \sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)$$

\subsubsection{Partial derivatives for components of $\theta$}

First let us focus on computing the gradient of the topic distribution.

$$ \pd{\theta_i}{ \log P(T|S,\theta,\phi) } =
\pd{\theta_i}{
\log \sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)}$$

We have that $\frac{d}{dx}\left[\log(f(x))\right] = \frac{1}{f(x)}\frac{df(x)}{dx}$

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\pd{\theta_i}{
\sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)}$$

Push inside the sum

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
\pd{\theta_i}{
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi} }
 \right)$$

Push across the prediction portion, which is constant with respect to $\theta$:

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
\pd{\theta_i}{
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta} }
 \prod_{(s,t)\in(S,T)}  P(t|s, z, \phi)
 \right)$$

Quotient rule: $\frac{d}{dx}\left[ \frac{f(x)}{g(x)} \right] = \frac{ \frac{df(x)}{dx} g(x) - f(x)\frac{dg(x)}{dx} }{\left(g(x)\right)^2}$. In this case, $f(x)$ is a density of exponential form so $f(x) = exp(h(x))$, and $g(x)$ is a partition function. Thus, the first term is the probability times the derivative of the density before exponentiating:
$\frac{\frac{df(x)}{dx} g{x}}{\left(g(x)\right)^2} = 
\frac{\frac{df(x)}{dx}}{g(x)} =
\frac{\frac{d \exp h(x)}{dx}}{g(x)} =
\frac{\exp h(x)\frac{d h(x)}{dx}}{g(x)} =
\frac{f(x)\frac{d h(x)}{dx}}{g(x)} =
p(x) \frac{d h(x)}{dx}$.
Regarding the second term, that turns into a probability times an expectation

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
	\left(
	P(z|S, \theta) \cdot F_i(S, z)
	-
	P(z|S, \theta) \sum_{z'} P(z'|S, \theta) F_i(S, z')
	\right)
 \prod_{(s,t)\in(S,T)}  P(t|s, z, \phi)
 \right)$$

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
	P(z|S, \theta)
	\left(
	F_i(S, z) - \E{z'| S, \theta}{F_i(S, z')}
	\right)
 \prod_{(s,t)\in(S,T)}  P(t|s, z, \phi)
 \right)$$


\subsubsection{Neat trick}

Taking the derivative of a complex product can lead to many terms.
Luckily there is a compact way to represent this product using some calculus:
\begin{eqnarray*}
\pd{\theta}{\prod_i f_i(\theta)} \\
 = \sum_i \prod_{j\ne i} f_j \pd{\theta}{f_i} \\
 = \sum_i \frac{1}{f_i} \prod_{j} f_j \pd{\theta}{f_i} \\
 = \sum_i \left( \prod_{j} f_j \right) \frac{1}{f_i} \pd{\theta}{f_i} \\
 = \left( \prod_{j} f_j \right) \sum_i \frac{1}{f_i} \pd{\theta}{f_i} \\
\end{eqnarray*}
But we know that $\pd{\theta}{\log f} = \frac{1}{f} \pd{\theta}{f}$, so we get:
$$
\pd{\theta}{\prod_i f_i(\theta)} =
\left( \prod_{i} f_i \right) \sum_i \pd{\theta}{\log f_i}$$
This is a much nicer expression to work with, especially in our log-linear models.

\subsubsection{Partial derivatives for components of $\phi$}

$$ \pd{\phi_i}{ \log P(T|S,\theta,\phi) } =
\pd{\phi_i}{
\log \sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)}$$

We have that $\frac{d}{dx}\left[\log(f(x))\right] = \frac{1}{f(x)}\frac{df(x)}{dx}$

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\pd{\phi_i}{
\sum_{z\in Z} \left( 
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi}
 \right)}$$

Push inside the sum

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
\pd{\phi_i}{
\frac{\exp(\theta \cdot F(S, z))}{Z_\theta}
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi} }
 \right)$$

Now the first term is constant

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
P(z|S, \theta)
\pd{\phi_i}{
 \prod_{(s,t)\in(S,T)} 
\frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi} }
 \right)$$

Then apply our trick

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
P(z|S, \theta)
\left( \prod_{(s,t)\in(S,T)} P(t|s, z, \phi) \right)
\sum_{(s,t)\in(S, T)}
\pd{\phi_i}{
	\log \frac{\exp(\phi \cdot G(s, z, t))}{Z_\phi} }
 \right)$$

$$ =
\frac{1}{P(T|S,\theta,\phi)}
\sum_{z\in Z} \left( 
	P(z|S, \theta)
	\left( \prod_{(s,t)\in(S,T)} P(t|s, z, \phi) \right)
	\sum_{(s,t)\in(S, T)}
	\left(
		G_i(s,z,t) - \E{t' | s, z}{G_i(s, z, t')}
	\right)
\right)
$$

\subsubsection{Complete gradient}

Consider a posterior distribution over each topic for a given document pair, defined as this:
$$
P(z|S, T, \theta, \phi) =
\frac
	%{P(z|s,\theta) \prod_{(s,t)\in(S, T)} P(t|s,z,\phi)}
	{P(T, z|S, \theta.\phi)}
	{P(T|S, \theta,\phi)}
$$

Then the gradients are just differences between empirical counts and true counts modulated by expectation under this distribution.
So we have:

$$
\pd
	{\theta_i}
	{\log P(T|S, \theta, \phi)}
=
\E{z \sim | S, T, \theta, \phi}
	{F_i(S, z) - \E{z' | S, \theta}{F_i(S, z')}}
$$

$$
\pd
	{\phi_i}
	{\log P(T|S, \theta, \phi)}
=
\E{z \sim | S, T, \theta, \phi}
	{\sum_{(s,t)\in(S,T)}
		\left(
			G_i(s, z, t) - \E{t' | s, z, \phi}{G_i(s, z, t')}
		\right)
	}
$$

Now for each document, we can first compute the log density of each topic under the current model, and normalize to get a distribution.
Then we gather statistics and multiply by this posterior as necessary.

\subsection{Optimization}

Currently we're exploring batch optimization using RProp (\emph{add citation!}).
Note that we have to initialize with a random vector.
A zero vector sits on a saddle point with respect to likelihood, and the model fails to make progress if we start there.
We incorporate an L2 norm as a regularizer, but its weight must be small (no larger than $0.1$) to allow sharp topic distributions.

\section{Simple example}

Say we have a corpus containing the following two French-English sentence pairs, where the subscript indicates the word alignment:

\begin{itemize}
\item[(1a)] le$_1$ r\'egime$_2$ d\'emocratique$_3$
\item[(1b)] the$_1$ democratic$_3$ regime$_2$
\end{itemize}

\begin{itemize}
\item[(2a)] le$_1$ r\'egime$_2$ pamplemousse$_3$
\item[(2b)] the$_1$ grapefruit$_3$ diet$_2$
\end{itemize}

If we learn two latent topics, the resulting document distribution looks like this:

\begin{tabular}{l|rr}
& topic 0 & topic 1 \\
\hline
sentence 1 & 0.01 & 0.99 \\
sentence 2 & 0.99 & 0.01 \\
\end{tabular}

The translation distribution is:

\begin{tabular}{ll|r|rr}
source & target & super-topic01 & topic0 & topic1 \\
\hline
\hline
le & the & 1.00 & 1.00 & 1.00 \\
\hline
r\'egime & regime & 0.45 & 0.99 & 0.01 \\
r\'egime & diet & 0.55 & 0.01 & 0.99 \\
\hline
d\'emocratique & democratic & 1.00 & 1.00 & 1.00 \\
\hline
pamplemousse & grapefruit & 1.00 & 1.00 & 1.00 \\
\end{tabular}

Clearly \emph{d\'emocratique} and \emph{pamplemousse} are able to significantly influence the topic distribution, even though their translation is not topic dependent.
Also note that the translation of \emph{r\'egime} is successfully disambiguated by the topic indicator.

Of course, this simple example could also be clearly learned by a phrasal model.
The broader point is that we can capture more global relationships with this model.

\end{document}
