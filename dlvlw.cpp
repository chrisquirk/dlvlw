#include "stdafx.h"

#include <cstdio>
#include <cstring>
#include <deque>
#include <exception>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <math.h>
#include <set>
#include <string>
#include <sstream>
#include <time.h>
#ifdef WIN32
#include <unordered_map>
#else
#include <ext/hash_map>
#endif
#include <utility>
#include <vector>

#define D if (false)

using namespace std;

/**
 * Convert a number to its string representation.
 */
string to_str(int i)
{
	ostringstream oss;
	oss << i;
	return oss.str();
}

/**
 * Exception thrown during failed read attempts
 */
class ReadError : public exception
{
public:
	virtual const char * what() { return "Error reading from file"; }
};

/**
 * Used for saving and loading objects in a binary format.
 */
class SerializationBuffer
{
public:
	/**
	 * Create a serialization buffer backed by the given filename
	 *
	 * @param filename Where data is stored
	 * @param write If true, create/overwrite the filename and open for writing; if false, open for read.
	 */
	SerializationBuffer(const char * filename, bool write)
		: m_pfile(fopen(filename, write ? "wb" : "rb"))
	{
	}

	/** Destructor, which closes the given file. */
	~SerializationBuffer()
	{
		fclose(m_pfile);
	}

	/** Read one integer from a read-only buffer. */
	int ReadInt() { int i; if (fread(&i, sizeof(int), 1, m_pfile) != 1) throw ReadError(); return i; }
	/** Write one integer to a write-only buffer. */
	void WriteInt(int i) { fwrite(&i, sizeof(int), 1, m_pfile); }
	/** Read one double from a read-only buffer. */
	double ReadDouble() { double d; if (fread(&d, sizeof(double), 1, m_pfile) != 1) throw ReadError(); return d; }
	/** Write one double to a write-only buffer. */
	void  WriteDouble(double d) { fwrite(&d, sizeof(double), 1, m_pfile); }
	/** Read one string from a read-only buffer. */
	string ReadString()
	{
		size_t s;
		size_t count = fread(&s, sizeof(size_t), 1, m_pfile);
		if (1 != count) throw ReadError();
		vector<char> vc;
		vc.resize(s + 1);
		count = fread(&vc[0], sizeof(char), s, m_pfile);
		if (s != count) throw ReadError();
		vc[s] = 0;
		return string(&vc[0]);
	}
	/** Write one string to a write-only buffer. */
	void WriteString(const string & s)
	{
		size_t l = s.length();
		fwrite(&l, sizeof(size_t), 1, m_pfile);
		fwrite(s.c_str(), sizeof(char), l, m_pfile);
	}

protected:
	FILE * m_pfile;
};


/**
 * Exception for when the saved version of a file does not match the version of the loading code.
 */
class mismatched_version : public exception
{
public:
	virtual const char * what() { return "version in file does not match that of code."; }
};

/**
 * Class used for checking versions when serializing.
 *
 * @tparam VERSION
 * 			This version is saved when Save is called, and checked for
 *          match at load time. If it doesn't match, @ref mismatched_version
 *          is thrown.
 */
template <int VERSION>
class Version
{
public:
	/** Construct a new object from scratch */
	Version() {}
	/** Load a new object from a serialization buffer. Check if it matches; throw ::mismatched_version if not */
	Version(SerializationBuffer & sb)
	{
		int v = sb.ReadInt();
		if (v != VERSION)
		{
			throw mismatched_version();
		}
	}

	/** Save VERSION to a serialization buffer. */
	void Save(SerializationBuffer & sb)
	{
		sb.WriteInt(VERSION);
	}
};

/** Compute a simple multiplicative hash of a string */
int hashString(const char * str)
{
	int hash = 0;
	while (*str)
		hash = hash * 37 + (int)*(str++);
	if (hash == 0) ++hash;
	return hash;
}

/**
 * Map strings to unique, compact integers, and vice versa.
 * Here, compact means that if N strings are added to the Vocab,
 * then their IDs range from 0 to N-1.
 */
class Vocab
{
public:
	typedef map<string, int> H;
	/** Construct a new, empty vocabulary */
	Vocab() {}

	/**
	 * Construct a new Vocab object from saved vocabulary in a buffer
	 * @param sb Buffer containing the vocab
	 */
	Vocab(SerializationBuffer & sb)
	{
		int c = sb.ReadInt();
		for (int i = 0; i < c; ++i)
			this->operator[](sb.ReadString());
	}

	/**
	 * Save vocabulary to buffer
	 * @param sb Where to write the buffer
	 */
	void Save(SerializationBuffer & sb)
	{
		sb.WriteInt(count());
		for (int i = 0; i < count(); ++ i)
			sb.WriteString(m_vstr[i]);
	}

	/** Number of words in the vocab */
	int count() { return (int)m_vstr.size(); }

	int Lookup(const string & s)
	{
		H::iterator it = m_hash.find(s);
		if (it == m_hash.end()) return -1;
		return it->second;
	}

	/** Get a vocab id for the given string. If not found, a new vocab ID is identified. */
	int operator[](const string & s)
	{
		H::iterator it = m_hash.find(s);
		if (it != m_hash.end()) return it->second;

		int i = (int)m_vstr.size();
		m_hash[s] = i;
		m_vstr.push_back(s);
		m_vhashes.push_back(hashString(s.c_str()));
		return i;
	}

	/** Get a string for the given vocab id. */
	const char * operator[](int i)
	{
		return m_vstr[i].c_str();
	}

	/**
	 * Get the hash of the string of the given vocab. This is equivalent
	 * to `hashString((*this)[i])` but should be faster.
	 */
	int stringHash(int i)
	{
		return m_vhashes[i];
	}

	H m_hash;
	vector<string> m_vstr;
	vector<int> m_vhashes;
};

/**
 * Used to indicate that a function has not or should not be implemented for this class.
 */
class NotImplmentedException : public exception
{
public:
	virtual const char * what() { return "Function is not implemented."; }
};

/**
 * A feature manager is in charge of mapping features to indices in a parameter
 * vector. There are multiple ways to do this, and the code that generates 
 * features uses this concept to allow flexibility.
 *
 * Note that this is an abstract concept and not actual code. Implementations
 * of feature managers should support the members below;
 */
class FeatureManagerConcept
{
public:
	/** A feature manager should have a type representing a feature */
	typedef int Feat;
	/** A feature manager should also have a type for pre-compiled string constants.
	 * @see FeatureManagerConcept::MakeConst */
	typedef int Const;

	/** needs a parameterless constructor */
	FeatureManagerConcept() { throw NotImplmentedException(); }
	/** needs a constructor from a saved file */
	FeatureManagerConcept(SerializationBuffer & s) { throw NotImplmentedException(); }

	/** Construct a pre-compiled constant from a string */
	Const MakeConst(const char * str) { throw NotImplmentedException(); }

	/** Construct an empty feature */
	Feat Empty() { throw NotImplmentedException(); }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const string & s) { throw NotImplmentedException(); }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const char * str) { throw NotImplmentedException(); }
	/** Append an integer to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, int i) { throw NotImplmentedException(); }
	/** Append a vocab id to a feature. Note that the feature `f` is changed.
     * @param f The feature to add
	 * @param i The ID from the vocabulary
	 * @param v The Vocabulary from which the ID is drawn
	 */
	void Append(Feat & f, int i, Vocab & v) { throw NotImplmentedException(); }
	/** Get an ID from the given feature. Ideally this ID is compact, but it may not be. */
	int ToId(const Feat & f, bool addNew) { throw NotImplmentedException(); }

	/** Get some string representaiton from the feature ID */
	const string operator[](int i) { throw NotImplmentedException(); }
};

/**
 * Implementation of the FeatureManagerConcept that uses strings for features.
 *
 * This is slow but makes feature extraction easier to debug.
 */
class StringFeatureManager
{
public:
	/** Features here are just strings */
	typedef string Feat;
	/** Constants are unchanged */
	typedef const char * Const;

	/** Create an empty feature manager */
	StringFeatureManager() {}
	/** Load a feature manager from the given buffer */
	StringFeatureManager(SerializationBuffer & s) : m_vocab(s) {}

	/** Save the feature manager to the given buffer */
	void Save(SerializationBuffer & s)
	{
		m_vocab.Save(s);
	}

	/** Construct a pre-compiled constant from a string */
	Const MakeConst(const char * str) { return str; }

	/** Construct an empty feature */
	Feat Empty() { return Feat(""); }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const string & s) { f += s; }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const char * str) { f += str; }
	/** Append an integer to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, int i) { f += to_str(i); }
	/** Append a vocab id to a feature. Note that the feature `f` is changed.
     * @param f The feature to add
	 * @param i The ID from the vocabulary
	 * @param v The Vocabulary from which the ID is drawn
	 */
	void Append(Feat & f, int i, Vocab & v) { f += v[i]; }
	/** Get an ID from the given feature. Ideally this ID is compact, but it may not be. */
	int ToId(const Feat & f, bool addNew)
	{
		if (addNew) return m_vocab[f];
		return m_vocab.Lookup(f);
	}

	/** Get some string representaiton from the feature ID */
	const string operator[](int i) { return m_vocab[i]; }

	/** Return the number of distinct features seen so far. */
	int count() { return m_vocab.count(); }

protected:
	Vocab m_vocab;
};

/** 
 * Feature manager that avoids string operations by just combining hashes.
 * However, it still maps the features into a compact set using a map
 * from int to int.  See the FeatureManagerConcept for more info.
 *
 * FIXME: this should use a hashtable, not a map!
 */
class CompactHashFeatureManager
{
public:
	/** Features are integers -- just hash values */
	typedef int Feat;
	/** String constants are integers -- just hash values */
	typedef int Const;

#ifdef WIN32
	typedef unordered_map<int, int> MAP;
#else
	typedef __gnu_cxx::hash_map<int, int> MAP;
#endif

	/** Construct an empty feature manager */
	CompactHashFeatureManager() {}
	/** Create a feature manager from a saved buffer */
	CompactHashFeatureManager(SerializationBuffer & s)
	{
		int count = s.ReadInt();
		for (int i = 0; i < count; ++i)
		{
			int k = s.ReadInt(), v = s.ReadInt();
			m_compact[k] = v;
		}
	}

	/** Write a feature manager to the given buffer */
	void Save(SerializationBuffer & s)
	{
		s.WriteInt(count());
		for (MAP::iterator it = m_compact.begin(); it != m_compact.end(); ++it)
		{
			s.WriteInt(it->first);
			s.WriteInt(it->second);
		}
	}

	/** Construct a pre-compiled constant from a string */
	Const MakeConst(const char * str) { return hashString(str); }

	/** Construct an empty feature */
	Feat Empty() { return 99999989; }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const string & s) { f = f * 104729 + hashString(s.c_str()); }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const char * str) { f = f * 104729 + hashString(str); }
	/** Append an integer to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const int i) { f = f * 104729 + i; }
	/** Append a vocab id to a feature. Note that the feature `f` is changed.
     * @param f The feature to add
	 * @param i The ID from the vocabulary
	 * @param v The Vocabulary from which the ID is drawn
	 */
	void Append(Feat & f, int i, Vocab & v) { f = f * 104729 + v.stringHash(i); }

	/** Return the number of distinct features seen so far. */
	int count() { return (int)m_compact.size(); }

	/** Get an ID from the given feature. Ideally this ID is compact, but it may not be. */
	int ToId(const Feat & f, bool addNew)
	{
		MAP::iterator it = m_compact.find(f);
		if (it != m_compact.end()) return it->second;
		if (!addNew) return -1;
		int val = (int)m_compact.size();
		m_compact[f] = val;
		return val;
	}

	/** Get some string representaiton from the feature ID */
	string operator[](int i) { return to_str(i); }

protected:
	MAP m_compact;
};

/**
 * Feature manager that uses hashes for speed, and approximates compactness
 * by only keeping the top few least significant bits. See FeatureManagerConcept
 * for more information. This should be both fast and uses no memory, but 
 * can cause collisions.
 *
 * @tparam BITS Number of least significant bits to keep. The parameter vector will
 * be \f$ <= 2^\textrm{BITS}\f$ in dimension.
 */
template <int BITS = 20>
class HashFeatureManager
{
public:
	/** Construct an empty feature manager */
	HashFeatureManager() {}
	/** Create a feature manager from a saved buffer -- nothing to do, really, except check version */
	HashFeatureManager(SerializationBuffer & s) : m_version(s) {}
	/** Write a feature manager to the given buffer  -- save the version number */
	void Save(SerializationBuffer & s) { m_version.Save(s); }

	/** Features are integers -- just BITS bits from their hash values */
	typedef int Feat;
	/** String constants are integers -- just hash values */
	typedef int Const;

	/** Construct a pre-compiled constant from a string */
	Const MakeConst(const char * str) { return hashString(str); }
	/** Construct an empty feature */
	Feat Empty() { return 0; }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const string & s) { f = f * 104729 + hashString(s.c_str()); }
	/** Append a string to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const char * str) { f = f * 104729 + hashString(str); }
	/** Append an integer to a feature. Note that the feature `f` is changed. */
	void Append(Feat & f, const int i) { f = f * 104729 + i; }
	/** Append a vocab id to a feature. Note that the feature `f` is changed.
     * @param f The feature to id
	 * @param i The ID from the vocabulary
	 * @param v The Vocabulary from which the ID is drawn
	 */
	void Append(Feat & f, int i, Vocab & v) { f = f * 104729 + v.stringHash(i); }
	/** Get an ID from the given feature. Ranges between 0 and 2^BITS - 1. */
	int ToId(const Feat & f)
	{
		return f & ((1 << BITS) - 1);
	}

	/** Return bound on distinct feature count */
	int count() { return 1 << BITS; }

	/** Get a string representaiton from the feature ID -- not the original feature name, just a string version of the hash*/
	string operator[](int i) { return to_str(i); }

protected:
	Version<BITS> m_version;
};

/**
 * Break a string into words, look up the vocab ids for each word, and store them.
 *
 * @param[in] str The string containing whitespace delimited tokens
 * @param[out] vi The resulting array of vocabulary IDs. Note that this is not cleared first -- we just append
 * @param[in] v The vocab used for mapping tokens to vocab IDs.
 */
void tokenize(const string & str, vector<int> & vi, Vocab & v)
{
	istringstream iss(str);
	string tok;
	while (iss >> tok)
	{
		vi.push_back(v[tok]);
	}
}

/**
 * Represents a single word-aligned sentence pair.
 */
class WordAlignment
{
public:
	/**
	 * Iterates over word pairs in the word alignment
	 */
	struct iterator
	{
		/** Construct an iterator pointing at the beginning of this alignment. */
		iterator(WordAlignment & wa) : m_wa(wa), m_it(wa.A.begin()) {}
		/** Construct an iterator pointing at some location in this alignment. */
		iterator(WordAlignment & wa, vector<pair<int, int> >::iterator it) : m_wa(wa), m_it(it) {}
		/** The word alignment which is being iterated */
		const WordAlignment & m_wa;
		/** Position in the alignment relation */
		vector<pair<int, int> >::iterator m_it;
		/** Check if two iterators are pointing at the same location */
		bool operator==(const iterator & it) const
		{
			return m_it == it.m_it;
		}
		/** Check if two iterators are not pointing at the same location */
		bool operator!=(const iterator & it) const
		{
			return m_it != it.m_it;
		}
		/** Advance the current iterator */
		iterator & operator++(int)
		{
			++m_it;
			return *this; 
		}
		/** Advance the current iterator */
		iterator operator++()
		{
			iterator it = *this;
			++m_it;
			return it;
		}
		/**
		 * Get the source and target word vocab IDs the current position.
		 *
		 * @returns  the vocab IDs in the current relation, not the positions.
		 */
		pair<int, int> operator*() const
		{
			pair<int, int> p = *m_it;
			return make_pair(m_wa.S[p.first], m_wa.T[p.second]);
		}
	};

	/**
	 * Construct a string from the Moses style representaiton
	 *
	 * @param s Whitespace-delimited list of source language tokens
	 * @param t Whitespace-delimited list of target language tokens
	 * @param a Whitespace-delimited list of alignments. Each alignment is a dash delimited pair of source and target indices.
	 * @param vS Source language vocabulary -- vocab IDs are stored, not strings.
	 * @param vT Source language vocabulary -- vocab IDs are stored, not strings.
	 *
	 * Sample input (s, t, a) in order:
	 *
	 * - je ne parle pas francais
	 * - I do not speak French
	 * - 0-0 1-2 2-3 3-2 4-2
	 * 
	 * In this case, we have the pairs:
	 *
	 * - je-I
	 * - ne-not
	 * - parle-speak
	 * - pas-not
	 * - francais-French
	 *
	 * There is also an implicit alignment of NULL-do that is not listed here.
	 */
	WordAlignment(const string & s, const string & t, const string & a, Vocab & vS, Vocab & vT)
	{
		tokenize(s, S, vS);
		tokenize(t, T, vT);
		
		istringstream iss(a);
		int i, j;
		char c;
		while (iss >> i >> c >> j)
		{
			A.push_back(make_pair(i, j));
		}
	}

	/**
	 * Write out the word-pairs in the current alignment
	 *
	 * @tparam STREAM Some stream that supports the `<<` operator.
	 * @param stream The stream to which output should be written.
	 * @param vs Source language vocabulary
	 * @param vt Target language vocabulary
	 */
	template <typename STREAM>
	void print(STREAM & stream, Vocab & vs, Vocab & vt)
	{
		for (iterator it = begin(); it != end(); ++it)
		{
			stream << vs[(*it).first] << "/" << vt[(*it).second] << endl;
		}
	}

	/** Get an iterator pointing at the first alignment */
	iterator begin() { return iterator(*this, A.begin()); }
	/** Get an iterator pointing past the last alignment */
	iterator end() { return iterator(*this, A.end()); }

	/** Source language word IDs */
	vector<int> S;
	/** Target language word IDs */
	vector<int> T;
	/** Alignment relation, expressed as index pairs */
	vector<pair<int, int> > A;
};

/**
 * This is a generic consumer of features. It could be used for
 * prediction (see DotProductConsumer), for updating the gradient
 * (see GradientUpdateConsumer), for computing the dimension of the
 * feature vector (see DimensionConsumer), etc.
 *
 * @tparam FM This is a feature manager; it should fit the FeatureManagerConcept concept.
 */
template <typename FM>
class IFeatureConsumer
{
public:
	/** Add a binary indicator feature */
	virtual void add(const typename FM::Feat & feat) = 0;
	/** Add a real-valued feature with a given value */
	virtual void add(const typename FM::Feat & feat, double val) = 0;
};

/**
 * Identifies the dimensionality of the given feature vector.
 *
 * @tparam FM This is a feature manager; it should fit the FeatureManagerConcept concept.
 */
template <typename FM>
class DimensionConsumer : public IFeatureConsumer<FM>
{
public:
	/** Construct a consumer that uses the given feature manager */
	DimensionConsumer(FM & f) : m_fm(f), m_max(-1) {}
	/** Add a binary indicator feature */
	virtual void add(const typename FM::Feat & feat)
	{
		int f = m_fm.ToId(feat, true);
		if (f > m_max) m_max = f;
	}
	/** Add a real-valued feature with a given value */
	virtual void add(const typename FM::Feat & feat, double)
	{
		int f = m_fm.ToId(feat, true);
		if (f > m_max) m_max = f;
	}

	/** Print the dimension of the vector */
	int Dim() { return m_max + 1; }
protected:
	FM & m_fm;
	int m_max;
};

/**
 * Feature consumer that computes the dot product on the fly as features are added.
 * It maintains an accumulator, initally zero, which represents the dot-product of
 * all features added so far.
 *
 * @tparam FM This is a feature manager; it should fit the FeatureManagerConcept concept.
 * @tparam V This is a parameter vector; it should have a method `double operator[](int i)` to retrieve the weight of a given parameter
 */
template <typename FM, typename V>
class DotProductConsumer : public IFeatureConsumer<FM>
{
public:
	/** Construct a consumer that uses the given feature manager and parameter vector */
	DotProductConsumer(FM & fm, V & params, bool addNew) : m_fm(fm), m_params(params), m_v(0), m_fAddNew(addNew) {}
	/** Reset the accumulator to zero */
	void reset()
	{
		D cout << "<<< " << 0 << endl;
		m_v = 0;
	}
	/** Get the current accumulator value */
	double operator *() const {
		D cout << "=== " << m_v << endl;
		return m_v;
	}

	/** Incorporate the given feature. In practice, this adds the corresponding parameter value to the accumulator. */
	virtual void add(const typename FM::Feat & feat)
	{
		D cout << "... adding " << feat << " = " << m_params[m_fm.ToId(feat, m_fAddNew)] << endl;
		m_v += m_params[m_fm.ToId(feat, m_fAddNew)];
	}

	/** Incorporate the given feature. In practice, this adds the corresponding parameter value multiplied by the feature value to the accumulator. */
	virtual void add(const typename FM::Feat & feat, double val)
	{
		m_v += m_params[m_fm.ToId(feat, m_fAddNew)] * val;
	}

protected:
	FM & m_fm;
	V & m_params;
	double m_v;
	bool m_fAddNew;
};

/**
 * Consumer that updates a gradient vector (or any vector, really) based on features.
 *
 * @tparam FM This is a feature manager; it should fit the FeatureManagerConcept concept.
 * @tparam V This is a parameter vector; it should have a method `double operator[](int i)` to get and set the weight of a given parameter
 */
template <typename FM, typename V>
class GradientUpdateConsumer : public IFeatureConsumer<FM>
{
public:
	/** construct a consumer that uses the given feature manager and gradient */
	GradientUpdateConsumer(FM & fm, V & gradient, double weightScalar = 1)
		: m_fm(fm), m_gradient(gradient), Weight(0), Scalar(weightScalar) {}

	void SetWeight(double d) { Weight = d * Scalar; }

	/** Update the gradient for this feature */
	virtual void add(const typename FM::Feat & feat)
	{
		m_gradient[m_fm.ToId(feat, true)] += Weight;
	}

	/** Update the gradient for this feature with the given value */
	virtual void add(const typename FM::Feat & feat, double val)
	{
		m_gradient[m_fm.ToId(feat, true)] += Weight * val;
	}

protected:
	FM & m_fm;
	V & m_gradient;
	double Scalar;
	/** When updating the gradient, this is the scalar for the update */
	double Weight;

};

/**
 * Consumer that updates a gradient vector (or any vector, really) based on features.
 *
 * @tparam FM This is a feature manager; it should fit the FeatureManagerConcept concept.
 * @tparam V This is a parameter vector; it should have a method `double operator[](int i)` to get and set the weight of a given parameter
 */
template <typename FM, typename V>
class DelayedGradientUpdateConsumer : public IFeatureConsumer<FM>
{
public:
	/** construct a consumer that uses the given feature manager and gradient */
	DelayedGradientUpdateConsumer(FM & fm, V & gradient, double weightScalar = 1)
		: m_fm(fm), m_gradient(gradient), Weight(0), Scalar(weightScalar) {}

	void SetWeight(double d) { Weight = d * Scalar; }

	/** Update the gradient for this feature */
	virtual void add(const typename FM::Feat & feat)
	{
		m_updates.push_back(make_pair(m_fm.ToId(feat, true), Weight));
	}

	/** Update the gradient for this feature with the given value */
	virtual void add(const typename FM::Feat & feat, double val)
	{
		m_updates.push_back(make_pair(m_fm.ToId(feat, true), Weight * val));
	}

	void Apply()
	{
		for (size_t s = 0; s < m_updates.size(); ++s)
		{
			m_gradient[m_updates[s].first] += m_updates[s].second;
		}
		m_updates.clear();
	}

protected:
	FM & m_fm;
	V & m_gradient;
	vector<pair<size_t, double> > m_updates;
	double Scalar;
	/** When updating the gradient, this is the scalar for the update */
	double Weight;

};

/** Return the sign of the given number -- either -1, 0, or 1 */
double sign(double d)
{
	if (d < 0) return -1;
	if (d > 0) return 1;
	return 0;
}

/**
 * A differentiable function should be able to compute the value and gradient at a given point.
 */
class DifferentiableFunctionConcept
{
public:
	/**
	 * Evaluate the function, and return the value and the gradient of that point.
	 *
	 * @param[in] w The vector of inputs to the function
	 * @param[out] grad The gradient of the function at that input
	 * @returns The value of the function at that input
	 */
	double ValueAndGradient(vector<double> & w, vector<double> & grad) { throw NotImplmentedException(); }
};

/**
 * A wrapper around a differentiable function that checks its gradient at the given point
 * using finite differences. This fulfills the DifferentiableFunctionConcept.
 *
 * This is a slow checker for whether a gradient is being correctly computed. It evaluates
 * the function at the given value, and saves the gradient that was at that code.
 *
 * Then it empirically estimates the gradient using the finite differences method. 
 * We know that the derivative is \f$f'(x) = lim_{h\rightarrow 0} \frac{f(x+h) - f(x)}{h}\f$.
 * We can approximate the derivative as \f$\frac{f(x + h) - f(x)}{h}\f$ and see how that compares
 * to the answer given by the compute value -- this is called the *forward difference*
 * because we add \f$h\f$ to \f$x\f$. This code instead uses the *central difference*:
 * \f$\frac{f(x + h) - f(x - h)}{2h}\f$ in an attempt to better estimate the gradient.
 *
 * Then we can compare the computed gradient to its empirical difference. If both the
 * absolute and relative differences exceed some threshold, an exception is thrown.
 *
 * It's a good method for catching bugs in authoring gradients.
 *
 * @tparam T this class should fulfill the DifferentiableFunctionConcept
 */
template <typename T>
class GradientChecker
{
public:
	/**
	 * Construct a checker over the differentiable function t
	 *
	 * @param t function to check
	 * @param pv feature vocabulary -- if non-null, it prints the string of the feature where the gradient check failed.
	 */
	GradientChecker(T & t, Vocab * pv = NULL) : m_t(t), step(0.001), maxAbsDiff(1e-5), maxRelDiff(1e-5), m_pvocab(pv) {}

	/** width of the step to take in each dimension -- this is \f$h\f$ in the documentation above */
	double step;
	/** maximum absolute difference before an exception is thrown */
	double maxAbsDiff;
	/** maximum relative difference before an exception is thrown */
	double maxRelDiff;

	/** evaluate the function at the given point, then check that the gradient is close to the empirical gradient estimate */
	double ValueAndGradient(vector<double> & w, vector<double> & grad)
	{
		// dummy storage for subsequent func evals.
		vector<double> dummy(grad.size());
		vector<double> v(w);
		// zero out the gradient
		for (size_t s = 0; s < grad.size(); ++s) grad[s] = 0;
		// get the value value
		double value = m_t.ValueAndGradient(w, grad);

		vector<double> finiteDifferencesGrad(grad.size());
		for (size_t s = 0; s < grad.size(); ++s)
		{
			// compute the finite differences estimate of the gradient for dimension s
			v[s] = w[s] + step;
			double plusVal = m_t.ValueAndGradient(v, dummy);
			v[s] = w[s] - step;
			double minusVal = m_t.ValueAndGradient(v, dummy);
			v[s] = w[s];
			finiteDifferencesGrad[s] = (plusVal - minusVal) / (2.0 * step);
			double diff = finiteDifferencesGrad[s] - grad[s];
			if (fabs(diff) > maxAbsDiff &&
				fabs(diff / (fabs(finiteDifferencesGrad[s]) + fabs(grad[s]))) > maxRelDiff)
			{
				cerr << "failed gradient check" << endl;
				cerr << " component " << s << " of " << grad.size() << endl;
				if (m_pvocab != NULL) cerr << " feature " << (*m_pvocab)[s] << endl;
				cerr << " finite diff grad " << finiteDifferencesGrad[s] << endl;
				cerr << " expected grad " << grad[s] << endl;
				cerr << " val " << value << endl;
				cerr << " plusVal " << plusVal << endl;
				cerr << " minusVal " << minusVal << endl;
				cerr << " stepsize " << step << endl;
				cerr << "----" << endl;
				//throw exception("failed gradient check!");
			}
		}

		return value;
	}

protected:
	T & m_t;
	Vocab * m_pvocab;
};

/** Controls the backtracking behavior in RProp */
enum Backtracking
{
	/** Original RProp -- retract the prior update; retroactively called RProp+ */
	PLUS,
	/** Original RProp -- just change direction when gradient changes; also known as RProp- */
	MINUS
};

/**
 * A function minimizer for differentiable functions. Quick to converge, fast to implement, but not
 * as accurate as L-BFGS.
 *
 * It maintains a strength of update for each dimension, and uses only the sign of the gradient, 
 * not its value, to adjust this strength.
 *
 * @tparam BACK If PLUS, use weight backtracking (RProp+); if MINUS don't use it (RProp-)
 * @tparam IMPROVED If true, use the iRProp variant of Igel and Huesken (2000);
 *      if false, use original version (Riedmiller and Braun, 1993)
 */
template <Backtracking BACK = MINUS, bool IMPROVED = true>
class RProp
{
public:
	/** Construct a new minimizer */
	RProp() : maxStrength(50), minStrength(0), initStrength(0.0125), etaPlus(1.2), etaMinus(0.5) {}
	/** Maximum strength update for each dimension */
	double maxStrength;
	/** Minimum strength update for each dimension */
	double minStrength;
	/** Initial strength update for each dimension */
	double initStrength;
	/** Multiplier for strength when derivative maintains the same direction (>1)*/
	double etaPlus;
	/** Multiplier for strength when derivative changes direction (<1) */
	double etaMinus;

	/**
	 * Minimize the given function
	 *
	 * @tparam FUNC A differentiable function, should fulfill DifferentiableFunctionConcept
	 * @param f The function to minimize
	 * @param[in] init Starting point for minimization
	 * @param[out] result Point at which apparent minimum was found
	 * @returns Minimal value of function
	 */
	template <typename FUNC>
	double Minimize(FUNC & f, vector<double> & init, vector<double> & result)
	{
		vector<double> gradient(init.size(), 0);
		deque<double> results;
		vector<double> lastUpdate(BACK == PLUS ? init.size() : 0, 0);
		vector<double> strength(init.size(), initStrength);
		vector<double> lastGrad(init.size(), 0);
		double priorVal = numeric_limits<double>::infinity();
		result = init;
		while (true)
		{
			for (size_t s = 0; s < gradient.size(); ++s) gradient[s] = 0;
			double val = f.ValueAndGradient(result, gradient);
			results.push_back(val);
			if (results.size() >= 10)
			{
				double impr = ((results.front() - results.back()) / results.size()) / results.back();
				cout << val << '\t' << impr << endl;
				if (impr < 1e-3) return results.back();
				while (results.size() > 10)
					results.pop_front();
			}
			else
			{
				cout << val << "\t---" << endl;
			}
			for (size_t s = 0; s < result.size(); ++s)
			{
				if (BACK == PLUS)
				{
					if (lastGrad[s] * gradient[s] > 0)
					{
						strength[s] = min(strength[s] * etaPlus, maxStrength);
						lastUpdate[s] = -sign(gradient[s]) * strength[s];
						result[s] += lastUpdate[s];
					}
					else if (lastGrad[s] * gradient[s] < 0)
					{
						strength[s] = max(strength[s] * etaMinus, minStrength);
						if (!IMPROVED || val > priorVal) result[s] -= lastUpdate[s];
						gradient[s] = 0;
					}
					else
					{
						lastUpdate[s] = -sign(gradient[s]) * strength[s];
						result[s] += lastUpdate[s];
					}
				}
				else if (BACK == MINUS)
				{
					if (lastGrad[s] * gradient[s] > 0)
					{
						strength[s] = min(strength[s] * etaPlus, maxStrength);
					}
					else if (lastGrad[s] * gradient[s] < 0)
					{
						strength[s] = max(strength[s] * etaMinus, minStrength);
						if (IMPROVED) gradient[s] = 0;
					}
					result[s] -= sign(gradient[s]) * strength[s];
				}
				else
				{
					throw NotImplmentedException();
				}
			}
			lastGrad.swap(gradient);
			if (IMPROVED) priorVal = val;
		}
	}
};

/**
 * Function minimizer that descends along the gradient with a fixed step size.
 */
class GradientDescent
{
public:
	/** Construct a minimizer with the given step size */
	GradientDescent(double size = 0.1) : stepsize(size) {}

	/** Scalar used to multiply the gradient when updating */
	double stepsize;
	/**
	 * Minimize the given function
	 *
	 * @tparam FUNC A differentiable function, should fulfill DifferentiableFunctionConcept
	 * @param f The function to minimize
	 * @param[in] init Starting point for minimization
	 * @param[out] result Point at which apparent minimum was found
	 * @returns Minimal value of function
	 */
	template <typename FUNC>
	double Minimize(FUNC & f, vector<double> & init, vector<double> & result)
	{
		vector<double> gradient(init.size());
		deque<double> results;
		result = init;
		while (true)
		{
			for (size_t s = 0; s < gradient.size(); ++s) gradient[s] = 0;
			double val = f.ValueAndGradient(result, gradient);
			results.push_back(val);
			if (results.size() >= 10)
			{
				double impr = ((results.front() - results.back()) / results.size()) / results.back();
				if (impr < 1e-3) return results.back();
				while (results.size() > 10)
					results.pop_front();
			}
			for (size_t s = 0; s < result.size(); ++s)
				result[s] -= gradient[s] * stepsize;
		}
	}
};

/**
 * Represents a single node in the topic tree.
 *
 * This code assumes that the number of topics \f$K\f$ is equal to \f$2^B\f$ for some
 * non-negative number \f$B\f$. Furthermore, we assume that the topics are arranged
 * in a balanced binary tree that shares parameters. So say we have four topics,
 * from 0 to 3. Then there are "super-topics" {01}, {23}, and {0123}.
 *
 * The code uses a funky encoding, where negative topic numbers represent the
 * branch nodes in the topic tree -- so {23} would be -1, {01} would be -2, and
 * {0123} would be -3.
 */
class Topic
{
public:
	/**
	 * Create a topic representative.
	 * @param z ID of the topic, ranging from 0 to K-1
	 * @param K Number of topics
	 */
	Topic(int z, int K) : m_z((short)z), m_K((short)K) {}

	/** Check if this is the root of the topic tree */
	bool IsRoot() const { return m_z + m_K - 1 == 0; }

	/** Get the parent of this topic */
	Topic Parent() const
	{
		int zz = m_z + m_K - 1;
		zz = (zz - 1) / 2;
		return Topic(zz - m_K + 1, m_K);
	}

	/** Get a unique ID for this topic node. If negative, then it represents a branch node */
	int ID() { return (int)m_z; }

	/** Return a non-zero ID suitable for multiplicative hashing */
	int HashID() { return m_z + m_K; }

private:
	/** topic ID in the 0 to K-1 range */
	short m_z;
	/** number of topics */
	short m_K;
};

/**
 * Extracts features for topic selection using information from the source document
 *
 * @tparam FM Feature manager, should fulfill FeatureManagerConcept
 */
template <typename FM>
class TopicFeats
{
public:
	/**
	 * Construct a new feature extractor
	 *
	 * @param fm Feature manager instance
	 * @param v Source language vocabulary
	 */
	TopicFeats(FM & fm, Vocab & v) : m_fm(fm), m_vocab(v) { m_id = m_fm.MakeConst("topic_"); m_divider = m_fm.MakeConst("_"); }

	/**
	 * Extract features.
	 *
	 * @param S The source sentence vocabulary IDs
	 * @param z The current topic
	 * @param pfc Receives the features as they are produced
	 */
	void operator()(vector<int> & S, Topic z, IFeatureConsumer<FM> * pfc)
	{
		typename FM::Feat s, ss;
		for(vector<int>::iterator i = S.begin(); i != S.end(); ++i)
		{
			Topic zz = z;
			ss = m_fm.Empty();
			m_fm.Append(ss, m_id);
			m_fm.Append(s, *i, m_vocab);
			m_fm.Append(s, m_divider);
			while (!zz.IsRoot())
			{
				s = ss;
				m_fm.Append(s, zz.HashID());
				pfc->add(s);
				zz = zz.Parent();
			}
		}
	}

protected:
	FM & m_fm;
	Vocab & m_vocab;
	typename FM::Const m_id, m_divider;
};

/**
 * Extracts features for translating a source word as a target word in a given topic.
 *
 * @tparam FM Feature manager, should fulfill FeatureManagerConcept
 */
template <typename FM>
class TransFeats
{
public:
	/**
	 * Construct a new feature extractor
	 *
	 * @param fm Feature manager instance
	 * @param vs Source language vocabulary
	 * @param vt Target language vocabulary
	 */
	TransFeats(FM & fm, Vocab & vs, Vocab & vt) : m_fm(fm), m_vS(vs), m_vT(vt)
	{
		m_id = fm.MakeConst("trans_");
		m_divider = fm.MakeConst("_");
	}

	/**
	 * Extract features.
	 *
	 * @param s The source language word ID
	 * @param z The current topic
	 * @param t The target language word ID
	 * @param pfc Receives the features as they are produced
	 */
	void operator()(int s, Topic z, int t, IFeatureConsumer<FM> * pfc)
	{
		typename FM::Feat f, ff;
		ff = m_fm.Empty();
		m_fm.Append(ff, m_id);
		m_fm.Append(ff, s, m_vS);
		m_fm.Append(ff, m_divider);
		m_fm.Append(ff, t, m_vT);
		m_fm.Append(ff, m_divider);
		while (true)
		{
			f = ff;
			m_fm.Append(f, z.HashID());
			pfc->add(f);
			if (z.IsRoot()) break;
			z = z.Parent();
		}
	}

protected:
	FM & m_fm;
	typename FM::Const m_id, m_divider;
	Vocab & m_vS;
	Vocab & m_vT;
};

/**
 * Represents a set of word alignment sentence pairs.
 */
class WordAlignedSet
{
public:
	/**
	 * Construct a new set
	 *
	 * @param s Filename of source language sentences, one sentence per line
	 * @param t Filename of target language sentences, one sentence per line
	 * @param a Filename of alignment relations, one relation per line. 
	 * @param vs Source language vocabulary
	 * @param vt Target language vocabulary
	 */
	WordAlignedSet(const string & s, const string & t, const string & a, Vocab & vs, Vocab & vt)
		: m_s(s), m_t(t), m_a(a), m_vs(vs), m_vt(vt)
	{
		ifstream ss(m_s.c_str()), tt(m_t.c_str()), aa(m_a.c_str());
		string sss, ttt, aaa;
		while (
			getline(ss, sss) &&
			getline(tt, ttt) &&
			getline(aa, aaa))
		{
			m_aligns.push_back(WordAlignment(sss, ttt, aaa, m_vs, m_vt));
		}
	}

	void Permute()
	{
		int N = (int)m_aligns.size();
		//srand((unsigned int)time(NULL));
		srand(1);
		for (int i = 0; i < N; ++i)
		{
			int j = (rand() % (N - i)) + i;
			std::swap(m_aligns[i], m_aligns[j]);
		}
	}

	/**
	 * Class to walk through the WordAlignment objects in a given set.
	 */
	class Enumerator
	{
	public:
		/**
		 * Construct a new enumerator.
		 *
		 * The enumerator starts out in an uninitialized state -- you need to
		 * call advance() before looking at the alignment.
		 */
		Enumerator(WordAlignedSet & wa)
			: m_aligns(wa.m_aligns)
		{
			pos = -1;
		}

		/**
		 * Try to move to the next alignment.
		 *
		 * @returns true if there was another alignment, false if we hit the end.
		 */
		bool advance()
		{
			return (++pos < (int)m_aligns.size());
		}

		/** Get the current alignment */
		WordAlignment & operator*() { return m_aligns[pos]; }
		/** Get the current alignment */
		WordAlignment * operator->() { return &m_aligns[pos]; }

	protected:
		vector<WordAlignment> & m_aligns;
		int pos;
	};

	/** Construct a new Enumerator. Note that the callee is responsible for deleting this object when finished. */
	Enumerator* NewEnumerator() { return new Enumerator(*this); }

	/** Source language vocabulary */
	Vocab & m_vs;
	/** Target language vocabulary */
	Vocab & m_vt;
protected:
	string m_s, m_t, m_a;
	vector<WordAlignment> m_aligns;
};

/**
 * This is a GEN function for translations -- that is, for a given source
 * word, it lists out the set of target words that could possibly be
 * translations of that word. For discriminative training, our goal is to
 * provide the highest weight to the correct word. In theory, we could
 * consider all possible target words as translations for the given
 * source word, but this is quite slow.
 */
class TransGen
{
public:
	/**
	 * Construct a new GEN function from the given set of word aligned sentence pairs.
	 *
	 * This walks through the alignments, and for each source word, finds the set of
	 * target words to which it was aligned. It then stores that in a compact form.
	 */
	TransGen(WordAlignedSet & was)
	{
		// find the set of possible associations
		vector<set<int> > vli;
		{
			int aligns = 0;
			auto_ptr<WordAlignedSet::Enumerator> it(was.NewEnumerator());
			while (it->advance())
			{
				++aligns;
				for (WordAlignment::iterator i = (*it)->begin(); i != (*it)->end(); ++i)
				{
					pair<int, int> p = *i;
					while (p.first >= (int)vli.size())
						vli.push_back(set<int>());
					vli[p.first].insert(p.second);
					D cout << was.m_vs[p.first] << " -> " << was.m_vt[p.second] << endl;
				}
			}
			cout << "read " << aligns << " alignments" << endl;
		}

		// reserve space for them all
		{
			size_t total = 0;
			for (vector<set<int> >::iterator it = vli.begin(); it != vli.end(); ++it)
				total += it->size();
			m_ts.resize(total);
			m_ss.resize(vli.size() + 1, m_ts.end());
			m_ssc.resize(vli.size());
		}

		// put them in the compact representation
		{
			vector<vector<int>::iterator>::iterator outitS = m_ss.begin();
			vector<size_t>::iterator outitSc = m_ssc.begin();
			vector<int>::iterator outitT = m_ts.begin();
			for (vector<set<int> >::iterator it = vli.begin(); it != vli.end(); ++it)
			{
				*(outitS++) = outitT;
				*(outitSc++) = it->size();
				for (set<int>::iterator it2 = it->begin(); it2 != it->end(); ++it2)
					*(outitT++) = *it2;
			}
		}

		D for (int s = 0; s < (int)(m_ss.size() - 1); ++s)
		{
			cout << "GEN(" << was.m_vs[s] << ") =";
			for (vector<int>::iterator it = begin(s); it != end(s); ++it)
				cout << " " << was.m_vt[*it];
			cout << endl;
		}
	}

	/**
	 * Copy constructor for the GEN function.
	 */
	TransGen(TransGen & tg)
	{
		m_ts = tg.m_ts;
		m_ssc = tg.m_ssc;
		vector<int>::iterator it = m_ts.begin();
		m_ss.resize(m_ssc.size() + 1);
		for (size_t s = 0; s < m_ssc.size(); ++s)
		{
			m_ss[s] = it;
			it += m_ssc[s];
		}
		m_ss[m_ssc.size()] = it;
	}

	/**
	 * Load a GEN function from the buffer.
	 */
	TransGen(SerializationBuffer & sb)
	{
		size_t tcount = (size_t)sb.ReadInt();
		m_ts.resize(tcount);
		for (size_t i = 0; i < tcount; ++i)
			m_ts[i] = sb.ReadInt();

		vector<int>::iterator it = m_ts.begin();
		size_t scount = (size_t)sb.ReadInt();
		m_ss.resize(scount + 1);
		m_ssc.resize(scount);
		for (size_t i = 0; i < scount; ++i)
		{
			m_ss[i] = it;
			size_t c = (size_t)sb.ReadInt();
			m_ssc[i] = c;
			it += c;
		}
		m_ss[m_ssc.size()] = it;
	}

	/**
	 * Save the GEN function.
	 */
	void Save(SerializationBuffer & sb)
	{
		sb.WriteInt((int)m_ts.size());
		for (size_t s = 0; s < m_ts.size(); ++s)
			sb.WriteInt(m_ts[s]);

		sb.WriteInt((int)m_ssc.size());
		for (size_t s = 0; s < m_ssc.size(); ++s)
			sb.WriteInt((int)m_ssc[s]);
	}

	/** Get a begin iterator over all target words for the given source word */
	vector<int>::iterator begin(int s)
	{
		if ((size_t)s >= m_ss.size()) return m_ts.end();
		return m_ss[s];
	}
	/** Get an end iterator over all target words for the given source word */
	vector<int>::iterator end(int s)
	{
		if ((size_t)s >= m_ss.size()) return m_ts.end();
		return m_ss[s + 1];
	}
	/** Get the number of translates in the GEN function for s. */
	size_t outcomes(int s) { if (s >= m_ssc.size()) return 0; return m_ssc[s]; }

protected:
	vector<int> m_ts;
	vector<vector<int>::iterator> m_ss;
	vector<size_t> m_ssc;
};

/**
 * Compute the softmax function: \f$log \sum_i exp(x_i)\f$.
 *
 * @tparam IT a forward iterator over doubles
 * @param begin the first element to softmax
 * @param end one past the last element to softmax
 */
template <typename IT>
double softmax(IT begin, IT end)
{
	double max = *begin;
	for (IT it = begin + 1; it != end; ++it)
		if (*it > max) max = *it;
	double sum = 0;
	for (IT it = begin; it != end; ++it)
		sum += exp(*it - max);
	return max + log(sum);
}

/**
 * Compute the softmax function: \f$log \sum_i exp(x_i)\f$.
 *
 * @tparam V a vector of doubles, or something that supports begin() and end() iterators
 */
template <typename V>
double softmax(const V & v)
{
	return softmax(v.begin(), v.end());
}

/**
 * Represents the training data, and allows computation of negative log-likelihood of that
 * data, as well as the gradient thereof. For information about model under which negative
 * log-likelihood is computed, see DiscLatentLexModel.
 *
 * @tparam FEATURE_MANAGER A feature manager; should fulfill FeatureManagerConcept
 */
template <typename FEATURE_MANAGER>
class LatentTopicTrainingData
{
public:
	// we allow the latent model to be our friend
	template <typename FM>
	friend class DiscLatentLexModel;

	/**
	 * Construct a new training data set.
	 *
	 * @param wa The set of word aligned pairs in the training data
	 * @param Z The number of topics
	 * @param tg The GEN function, which specifies the set of translations available for each source word
	 * @param fm The feature manager
	 */
	LatentTopicTrainingData(WordAlignedSet & wa, int Z, TransGen & tg, FEATURE_MANAGER & fm)
		: m_wa(wa)
		, m_vs(wa.m_vs)
		, m_vt(wa.m_vt)
		, m_topicfeats(fm, wa.m_vs)
		, m_transfeats(fm, wa.m_vs, wa.m_vt)
		, m_Z(Z)
		, m_fm(fm)
		, m_gen(tg)
	{
		// first compute the dimension of the feature vector.
		cout << "estimating dimension" << endl;
		DimensionConsumer<FEATURE_MANAGER> dc(m_fm);
		{
			int count = 0;
			auto_ptr<WordAlignedSet::Enumerator> it(m_wa.NewEnumerator());
			while (it->advance())
			{
				for (int z = 0; z < m_Z; ++z)
				{
					Topic topic(z, m_Z);

					m_topicfeats((**it).S, topic, &dc);
					for (WordAlignment::iterator it2 = (*it)->begin(); it2 != (*it)->end(); ++it2)
					{
						int s = (*it2).first;
						for (vector<int>::iterator it3 = m_gen.begin(s); it3 != m_gen.end(s); ++it3)
							m_transfeats(s, topic, *it3, &dc);
					}
				}
				++count;
				if (count % 100 == 0) { cerr << count << " alignments\r"; cerr.flush(); }
			}
		}
		m_dim = dc.Dim();
	}

	/** Return the dimension of this dataset -- the number of parameters necessary in the parameter vector. */
	int Dimension() const { return m_dim; }

	/**
	 * Compute the negative log-likelihood of the data under parameters w; see DiscLatentLexModel for more information.
	 *
	 * @param[in] w Current parameters
	 * @param[out] grad Gradient of the negative log-likelihood
	 * @returns The negative log-likelihood of the data.
	 */
	double ValueAndGradient(vector<double> & w, vector<double> & grad)
	{
		if (m_supervision.size() > 0)
		{
			return ValueAndGradient_Supervised(w, grad);
		}

		// this is tricky. you may want to consult the lat_log_reg.pdf in the doc
		// subdirectory for information on how this gradient is computed.

		// theta is P(z|S) -- the distribution over topics based on source sentence
		vector<double> theta((size_t)m_Z);
		// rho is P(z|S,T) -- the posterior over topics after we see both source and target sentence,
		// and renormalize
		vector<double> rho((size_t)m_Z);
		// beta is P(t|s,z) -- the translation distribution conditioned on topics  
		vector<double> beta;

		//printFeats(w);

		GradientUpdateConsumer<FEATURE_MANAGER, vector<double> > guc(m_fm, grad);

		auto_ptr<WordAlignedSet::Enumerator> it(m_wa.NewEnumerator());
		double logprob = 0;
		int alignCount = 0;
		while (it->advance())
		{
			WordAlignment & wa = **it;
			logprob += ValueAndGradient_SingleExample(w, guc, wa, theta, rho, beta);
			++alignCount;
			if (0 == alignCount % 100) { cerr << alignCount << "\r"; cerr.flush(); }
		}
		D cout << -logprob << endl;
		return -logprob;
	}

	template <typename CONSUMER>
	double ValueAndGradient_SingleExample(vector<double> & w, CONSUMER & guc, WordAlignment & wa, vector<double> & theta, vector<double> & rho, vector<double> & beta)
	{
		double logprob = 0;
		size_t correct, count;

		D wa.print(cout, m_vs, m_vt);
		LogTopicDist(w, wa, theta);
		for (int z = 0; z < m_Z; ++z)
		{
			D cout << "log theta[" << z << "] = " << theta[z] << endl;
			rho[z] = theta[z];
			for (WordAlignment::iterator it2 = wa.begin(); it2 != wa.end(); ++it2)
			{
				LogTransDist(w, (*it2).first, z, (*it2).second, beta, count, correct);
				D cout << "log beta[" << m_vs[(*it2).first] << ", " << z << ", " << m_vt[(*it2).second] << "] = " << beta[correct] << endl;
				rho[z] += beta[correct];
			}
			D cout << "before norm " << rho[z] << endl;
		}
		double rhoZ = softmax(rho);
		D cout << "logprob = " << rhoZ << endl;
		logprob += rhoZ;
		for (int z = 0; z < m_Z; ++z)
		{
			rho[z] = exp(rho[z] - rhoZ);
			theta[z] = exp(theta[z]);
			D cout << "rho[" << z << "] = " << rho[z] << endl;
		}

		for (int z = 0; z < m_Z; ++z)
		{
			Topic topic(z, m_Z);
			//guc.SetWeight( theta[z] - rho[z] );
			//m_topicfeats(wa.S, topic, &guc);

			for (WordAlignment::iterator it2 = wa.begin(); it2 != wa.end(); ++it2)
			{
				int s = (*it2).first;
				int tCorrect = (*it2).second;
				LogTransDist(w, s, z, tCorrect, beta, count, correct);
				size_t index = 0;
				for (vector<int>::iterator itT = m_gen.begin(s); itT != m_gen.end(s); ++itT, ++index)
				{
					guc.SetWeight( rho[z] * (exp(beta[index]) - (index == correct ? 1.0 : 0.0)) );
					m_transfeats(s, topic, *itT, &guc);
				}
			}
		}
		for (int z = 0; z < m_Z; ++z)
		{
			Topic topic(z, m_Z);
			guc.SetWeight( theta[z] - rho[z] );
			m_topicfeats(wa.S, topic, &guc);
		}
		return logprob;
	}

	double ValueAndGradient_Supervised(vector<double> & w, vector<double> & grad)
	{
		// theta is P(z|S) -- the distribution over topics based on source sentence
		vector<double> theta((size_t)m_Z);
		// rho is P(z|S,T) -- the posterior over topics after we see both source and target sentence,
		// and renormalize
		vector<double> rho((size_t)m_Z);
		// beta is P(t|s,z) -- the translation distribution conditioned on topics  
		vector<double> beta;
		size_t correct, count;

		//printFeats(w);

		GradientUpdateConsumer<FEATURE_MANAGER, vector<double> > guc(m_fm, grad);

		auto_ptr<WordAlignedSet::Enumerator> it(m_wa.NewEnumerator());
		double logprob = 0;
		int alignCount = 0;
		while (it->advance())
		{
			int zCorrect = m_supervision[alignCount];
			D (*it)->print(cout, m_vs, m_vt);
			LogTopicDist(w, **it, theta);
			logprob += theta[zCorrect];
			for (int z = 0; z < m_Z; ++z)
			{
				Topic topic(z, m_Z);
				guc.SetWeight( exp(theta[z]) - (z == zCorrect ? 1.0 : 0.0) );
				m_topicfeats((**it).S, topic, &guc);
			}

			{
				Topic topic(zCorrect, m_Z);

				for (WordAlignment::iterator it2 = (*it)->begin(); it2 != (*it)->end(); ++it2)
				{
					int s = (*it2).first;
					int tCorrect = (*it2).second;
					LogTransDist(w, s, zCorrect, tCorrect, beta, count, correct);
					logprob += beta[correct];
					size_t index = 0;
					for (vector<int>::iterator itT = m_gen.begin(s); itT != m_gen.end(s); ++itT, ++index)
					{
						guc.SetWeight( (exp(beta[index]) - (index == correct ? 1.0 : 0.0)) );
						m_transfeats(s, topic, *itT, &guc);
					}
				}
			}
			++alignCount;
			if (0 == alignCount % 100) { cerr << alignCount << "\r"; cerr.flush(); }
		}
		D cout << -logprob << endl;
		return -logprob;
	}

	/**
	 * Find the logprob of each topic for the current doc
	 *
	 * @param[in] params Parameter vector for topic distribution
	 * @param[in] wa Current document
	 * @param[out] theta logprob of each topic
	 * @returns The log of the partition function.
	 */
	void LogTopicDist(vector<double> & params, WordAlignment & wa, vector<double> & theta)
	{
		if ((int)theta.size() != m_Z) theta.resize(m_Z);
		DotProductConsumer<FEATURE_MANAGER, vector<double> > dpc(m_fm, params, true);
		for (int z = 0; z < m_Z; ++z)
		{
			Topic t(z, m_Z);
			m_topicfeats(wa.S, t, &dpc);
			theta[z] = *dpc;
			dpc.reset();
		}
		double logZ = softmax(theta);
		for (int z = 0; z < m_Z; ++z) theta[z] -= logZ;
	}

	/**
	 * Find the logprob of each translation for the given word in the given topic.
	 *
	 * @param params Current parameter vector for log-linear distribution.
	 * @param s Source word
	 * @param z Current topic
	 * @param tCorrect The correct target translation
	 * @param[out] beta The logprob of each translation
	 * @param[out] count The number of relevant entries in beta
	 * @param[out] correct The index of the correct translation in beta
	 * @returns the log of the partition function.
	 */
	double LogTransDist(vector<double> & params, int s, int z, int tCorrect, vector<double> & beta, size_t & count, size_t & correct)
	{
		count = m_gen.outcomes(s);
		if (beta.size() < count) beta.resize(count);
		DotProductConsumer<FEATURE_MANAGER, vector<double> > dpc(m_fm, params, true);
		size_t offset = 0;
		for (vector<int>::iterator it = m_gen.begin(s); it != m_gen.end(s); ++it, ++offset)
		{
			int t = *it;
			if (t == tCorrect)
			{
				correct = offset;
				D cout << "CORRECT" << endl;
			}
			else
			{
				D cout << "incorrect" << endl;
			}
			m_transfeats(s, Topic(z, m_Z), t, &dpc);
			beta[offset] = *dpc;
			dpc.reset();
		}

		double logZ = softmax(&beta[0], (&beta[0]) + count);
		D cout << "log Z = " << logZ << endl;
		for (offset = 0; offset < count; ++offset)
		{
			beta[offset] -= logZ;
			D cout << "prob = " << exp(beta[offset]) << endl;
		}
		return logZ;
	}

	/** Print a labeled version of the parameter vector.  */
	void printFeats(vector<double> & params)
	{
		for (int i = 0; i < m_fm.count(); ++i)
		{
			cout << params[i] << "\t" << m_fm[i] << endl;
		}
	}

	/** Get the feature maanger */
	FEATURE_MANAGER * FeatureManager() { return &m_fm; }

protected:
	WordAlignedSet & m_wa;
	Vocab & m_vs;
	Vocab & m_vt;
	TopicFeats<FEATURE_MANAGER> m_topicfeats;
	TransFeats<FEATURE_MANAGER> m_transfeats;
	TransGen & m_gen;
	FEATURE_MANAGER & m_fm;
	int m_Z;
	int m_dim;
	vector<int> m_supervision;
};

/**
 * Computes the L2 norm of the paramters as a DifferentiableFunctionConcept.
 *
 * This should be \f$\alpha \cdot \sum_i (x_i - c_i)^2\f$. Here \f$\alpha\f$ is a scale
 * factor for the norm (1 is often a good choice), and \f$c_i\f$ is the "center"
 * to which we regularize -- often 0 is used.
 */
class L2Norm
{
public:
	/** Construct a norm centered at the given point */
	L2Norm(double weight, vector<double> & center) : m_weight(weight), m_center(center) {}
	/** Construct a norm centered at zero */
	L2Norm(double weight) : m_weight(weight), m_center() {}

	/** return the dimension of this function */
	size_t Dimension() { return m_center.size(); }

	/** Return value and gradient at given point. */
	double ValueAndGradient(vector<double> & w, vector<double> & grad)
	{
		double value = 0;
		for (size_t s = 0; s < w.size(); ++s)
		{
			double center = (s >= m_center.size() ? 0 : m_center[s]);
			double diff = w[s] - center;
			value += diff * diff;
			grad[s] += 2 * diff * m_weight;
		}
		return value * m_weight;
	}

protected:
	vector<double> m_center;
	double m_weight;
};

/**
 * Compute the sum of two differentiable functions as a DifferentiableFunctionConcept
 *
 * @tparam F the first differentiable function
 * @tparam G the first differentiable function
 */
template <typename F, typename G>
class FunctionSum
{
public:
	/** Construct the sum of these two funcs */
	FunctionSum(F & f, G & g) : m_f(f), m_g(g) {}

	/** Compute the value and gradient of the sum */
	double ValueAndGradient(vector<double> & w, vector<double> & grad)
	{
		double d1 = m_f.ValueAndGradient(w, grad);
		double d2 = m_g.ValueAndGradient(w, grad);
		cout << d1 << "\t" << d2 << "\t";
		return d1 + d2;
	}

protected:
	F & m_f;
	G & m_g;
};

/**
 * Model for predicting latent topics conditioned on source, and for translating
 * conditioned on those topics.
 *
 * Let S be the source document, consisting of source words s. Each source word
 * is aligned to some target word t. We have a set of latent topics z. The
 * likelihood of the data is \f$\sum_z \left( P(z|S) \cdot \prod_s P(t|s,z) \right)\f$. That is,
 * we first predict the topic based on the source document, and then predict
 * each target word based on the topic and its source word. It's a mixture model:
 * we sum out over these latent topics.
 *
 * Both \f$P(z|S)\f$ and \f$P(t|s,z)\f$ are log-linear distributions, learned on some
 * word-aligned parallel data.
 *
 * @tparam FEATURE_MANAGER This maps features to parameter vector indices, following the FeatureManagerConcept.
 */
template <typename FEATURE_MANAGER>
class DiscLatentLexModel
{
public:
	/**
	 * Train a model using the given corpus and number of topics.
	 */
	static DiscLatentLexModel * Train(string & s, string & t, string & a, int topics)
	{
		cout << "Training with " << topics << " topic(s)" << endl;
		Vocab vs, vt;
		FEATURE_MANAGER fm;
		cout << "Constructing word aligned set" << endl;
		WordAlignedSet was(s, t, a, vs, vt);
		was.Permute();

		cout << "Constructing gen" << endl;
		TransGen gen(was);
		cout << "Constructing data" << endl;
		LatentTopicTrainingData<FEATURE_MANAGER> d(was, topics, gen, fm);
		L2Norm regularizer(1);
		//GradientChecker<Model> gc(m, m.FeatureVocab());
		//FunctionSum<GradientChecker<Model>, L2Norm> objective(gc, regularizer);
		FunctionSum<LatentTopicTrainingData<FEATURE_MANAGER>, L2Norm> objective(d, regularizer);

		size_t dim = d.Dimension();
		cout << "dimension = " << dim << endl;

		vector<double> init(dim), result(dim);
		for (size_t i = 0; i < dim; ++i) init[i] = 4 * (rand() / double(RAND_MAX) - 0.5);

		//GradientDescent minimizer;
		RProp<> minimizer;
		cout << "optimizing" << endl;
		double negloglike = minimizer.Minimize(objective, init, result);
		cout << "found minimum value of " << negloglike << endl;

		return new DiscLatentLexModel(d, result);
	}

	/**
	 * Train a model using the given corpus and number of topics.
	 */
	static DiscLatentLexModel * TrainSgd(string & s, string & t, string & a, int topics)
	{
		cout << "Training with " << topics << " topic(s)" << endl;
		Vocab vs, vt;
		FEATURE_MANAGER fm;
		cout << "Constructing word aligned set" << endl;
		WordAlignedSet was(s, t, a, vs, vt);
		was.Permute();

		cout << "Constructing gen" << endl;
		TransGen gen(was);
		cout << "Constructing data" << endl;
		LatentTopicTrainingData<FEATURE_MANAGER> d(was, topics, gen, fm);

		size_t dim = d.Dimension();
		cout << "dimension = " << dim << endl;

		vector<double> w(dim);
		//vector<double> grad(dim);
		//for (size_t i = 0; i < dim; ++i) w[i] = 4 * (rand() / double(RAND_MAX) - 0.5);
		for (size_t i = 0; i < dim; ++i) w[i] = 0;

		for (int toptop = 1; toptop <= topics; toptop *= 2)
		{
			cout << endl;
			for (int z = 0; z < toptop; ++z)
			{
				Topic t(z, toptop);
				cout << z << ":";
				while (true) { cout << " " << t.HashID(); if (t.IsRoot()) break; t = t.Parent(); }
				cout << endl;
			}
			vector<double> theta((size_t)toptop);
			vector<double> rho((size_t)toptop);
			vector<double> beta;
			LatentTopicTrainingData<FEATURE_MANAGER> dd(was, toptop, gen, fm);
			cout << "dimension = " << dd.Dimension() << endl;
			for (int it = 1; it <= 10; ++it)
			{
				double updateWeight = -1.0 / (0 + it);
				GradientUpdateConsumer<FEATURE_MANAGER, vector<double> > guc(fm, w, updateWeight);
				auto_ptr<WordAlignedSet::Enumerator> e(was.NewEnumerator());
				double logprob = 0;
				int c = 0;
				while (e->advance())
				{
					logprob += dd.ValueAndGradient_SingleExample(w, guc, **e, theta, rho, beta);
					++c;
				}
				printf("%f\n", logprob);
			}
		}

		return new DiscLatentLexModel(d, w);
	}

	/** Construct a model using the given data and parameters.  */
	DiscLatentLexModel(LatentTopicTrainingData<FEATURE_MANAGER> & data, vector<double> & params)
		: m_Z(data.m_Z)
		, m_vs(data.m_vs)
		, m_vt(data.m_vt)
		, m_fm(data.m_fm)
		, m_gen(data.m_gen)
		, m_topicfeats(m_fm, m_vs)
		, m_transfeats(m_fm, m_vs, m_vt)
		, m_params(params)
	{
	}

	/** Load a saved model from the given buffer */
	DiscLatentLexModel(SerializationBuffer & sb)
		: m_version(sb)
		, m_vs(sb)
		, m_vt(sb)
		, m_fm(sb)
		, m_gen(sb)
		, m_topicfeats(m_fm, m_vs)
		, m_transfeats(m_fm, m_vs, m_vt)
	{
		m_Z = sb.ReadInt();
		size_t paramLen = (size_t)sb.ReadInt();
		m_params.resize(paramLen);
		for (size_t s = 0; s < paramLen; ++s)
			m_params[s] = sb.ReadDouble();
	}

	/** Save a model to the given buffer */
	void Save(SerializationBuffer & sb)
	{
		m_version.Save(sb);
		m_vs.Save(sb);
		m_vt.Save(sb);
		m_fm.Save(sb);
		m_gen.Save(sb);
		sb.WriteInt(m_Z);
		sb.WriteInt((int)m_params.size());
		for (size_t s = 0; s < m_params.size(); ++s)
			sb.WriteDouble(m_params[s]);
	}

	/** Write the per-document (per-sentence) topic distributions to the given file. */
	void WriteDocumentPredictions(WordAlignedSet & was, const char * filename)
	{
		ofstream docpredicts(filename);
		auto_ptr<WordAlignedSet::Enumerator> e(was.NewEnumerator());
		vector<double> theta(Topics());
		while (e->advance())
		{
			WordAlignment & wa = **e;
			LogTopicDist(wa.S, theta);
			for (size_t i = 0; i < theta.size(); ++i)
			{
				if (i > 0) docpredicts << "\t";
				docpredicts << exp(theta[i]);
			}
			docpredicts << endl;
		}
	}

	/**
	 * Compute the log-likelihood of the given alignments
	 *
	 * @param was Set of word aligned pairs over which likelihood is computed
	 * @param[out] wordCount Number of words in the set
	 * @param[out] oovCount Number of out-of-vocabulary words in the set
	 */
	double LogLikelihood(WordAlignedSet & was, int & wordCount, int & oovCount)
	{
		oovCount = 0;
		wordCount = 0;
		auto_ptr<WordAlignedSet::Enumerator> e(was.NewEnumerator());
		vector<double> theta(Topics());
		vector<double> beta;
		vector<int> vt;
		size_t count;
		double logprob = 0;
		while (e->advance())
		{
			WordAlignment & wa = **e;
			LogTopicDist(wa.S, theta);
			for (size_t i = 0; i < theta.size(); ++i)
			{
				Topic z((int)i, m_Z);
				for (WordAlignment::iterator it2 = wa.begin(); it2 != wa.end(); ++it2)
				{
					if (i == 0)
					{
						++oovCount;
						++wordCount;
					}
					bool found = false;
					LogTransDist((*it2).first, z, vt, beta, count);
					for (size_t s = 0; s < count; ++s)
						if (vt[s] == (*it2).second)
						{
							found = true;
							theta[i] += beta[s];
							break;
						}
					if (i == 0 && found) --oovCount;
				}
			}
			logprob += softmax(theta);
		}
		return logprob;
	}

	/**
	 * Compute \f$log(P(z|S))\f$ for all \f$z \in 1..K\f$.
	 *
	 * @param[in] S the source language word ids
	 * @param[out] theta the log probabilities for each topic
	 */
	void LogTopicDist(vector<int> & S, vector<double> & theta)
	{
		if ((int)theta.size() != m_Z) theta.resize(m_Z);
		DotProductConsumer<FEATURE_MANAGER, vector<double> > dpc(m_fm, m_params, false);
		for (int z = 0; z < m_Z; ++z)
		{
			m_topicfeats(S, Topic(z, m_Z), &dpc);
			theta[z] = *dpc;
			dpc.reset();
		}
		double logZ = softmax(theta);
		for (int z = 0; z < m_Z; ++z) theta[z] -= logZ;
	}

	/** Write out \f$P(t|s,z)\f$ to a file */
	void WriteWordDistribution(const char * filename)
	{
		Vocab & S = m_vs;
		Vocab & T = m_vt;
		ofstream dist(filename);
		vector<int> vt;
		vector<double> beta;
		size_t count;
		double thresh = log(0.01);
		for (int s = 0; s < S.count(); ++s)
		{
			for (int z = -m_Z + 1; z < m_Z; ++z)
			{
				Topic topic(z, m_Z);
				LogTransDist(s, topic, vt, beta, count);
				for (size_t i = 0; i < count; ++i)
				{
					if (beta[i] > thresh)
						dist << S[s] << '\t' << z << '\t' << T[vt[i]] << '\t' << exp(beta[i]) << endl;
				}
			}
		}
	}

	/**
	 * Find the logprob of each translation for the given word in the given topic.
	 *
	 * @param s Source word
	 * @param z Current topic
	 * @param[out] vt The set of target words that are translations of s.
	 * @param[out] beta The logprob of each translation
	 * @param[out] count The number of relevant entries in beta
	 * @returns the log of the partition function.
	 */
	void LogTransDist(int s, Topic z, vector<int> & vt, vector<double> & beta, size_t & count)
	{
		count = m_gen.outcomes(s);
		if (count == 0) return;
		if (beta.size() < count) beta.resize(count);
		if (vt.size() < count) vt.resize(count);
		DotProductConsumer<FEATURE_MANAGER, vector<double> > dpc(m_fm, m_params, false);
		size_t index = 0;
		for (vector<int>::iterator it = m_gen.begin(s); it != m_gen.end(s); ++it, ++index)
		{
			vt[index] = *it;
			m_transfeats(s, z, *it, &dpc);
			beta[index] = *dpc;
			dpc.reset();
		}
		double logZ = softmax(&beta[0], (&beta[0]) + count);
		for (index = 0; index < count; ++index) beta[index] -= logZ;
	}

	/** Write out the current parameters to stdout */
	void PrintParameters()
	{
		for (int i = 0; i < m_fm.count(); ++i)
		{
			cout << m_params[i] << "\t" << m_fm[i] << endl;
		}
	}

	Vocab * SourceVocab() { return & m_vs; }
	Vocab * TargetVocab() { return & m_vt; }
	int Topics() { return m_Z; }

protected:
	Version<1> m_version;
	int m_Z;
	Vocab m_vs, m_vt;
	FEATURE_MANAGER m_fm;
	TransGen m_gen;
	TopicFeats<FEATURE_MANAGER> m_topicfeats;
	TransFeats<FEATURE_MANAGER> m_transfeats;
	vector<double> m_params;
};

typedef DiscLatentLexModel<CompactHashFeatureManager> Model;

/**
 * Train a topic model from the given data, and save it. 
 *
 * @param ss Source side of word-aligned parallel sentences
 * @param tt Target side of word-aligned parallel sentences
 * @param aa Word alignment relations
 * @param topics Number of topics to use -- should be a power of 2
 * @param modelfile The resulting binary model is saved here
 * @param docdist The distribution over topics for each document, one line per document, one double per document, is written here
 * @param worddist The distribution over target words give source word and topic is written here
 */
void Train(char* ss, char* tt, char* aa, int topics, char* modelfile, char* docdist, char* worddist)
{
	string s(ss), t(tt), a(aa);
	auto_ptr<Model> model(Model::Train(s, t, a, topics));
	//model->PrintParameters();
	{
		SerializationBuffer sb(modelfile, true);
		model->Save(sb);
	}
	WordAlignedSet was(s, t, a, *model->SourceVocab(), *model->TargetVocab());
	model->WriteDocumentPredictions(was, docdist);
	model->WriteWordDistribution(worddist);
}

/**
 * Train a topic model from the given data, and save it. 
 *
 * @param ss Source side of word-aligned parallel sentences
 * @param tt Target side of word-aligned parallel sentences
 * @param aa Word alignment relations
 * @param topics Number of topics to use -- should be a power of 2
 * @param modelfile The resulting binary model is saved here
 * @param docdist The distribution over topics for each document, one line per document, one double per document, is written here
 * @param worddist The distribution over target words give source word and topic is written here
 */
void TrainSgd(char* ss, char* tt, char* aa, int topics, char* modelfile, char* docdist, char* worddist)
{
	string s(ss), t(tt), a(aa);
	auto_ptr<Model> model(Model::TrainSgd(s, t, a, topics));
	//model->PrintParameters();
	{
		SerializationBuffer sb(modelfile, true);
		model->Save(sb);
	}
	WordAlignedSet was(s, t, a, *model->SourceVocab(), *model->TargetVocab());
	model->WriteDocumentPredictions(was, docdist);
	model->WriteWordDistribution(worddist);
}

/**
 * Load a saved topic model and test it on some data.
 * The log-likelihood is written to stdout.
 */
void Test(char* modelfile, char* ss, char* tt, char* aa, char* docdist)
{
	SerializationBuffer sb(modelfile, false);
	Model model(sb);

	string s(ss), t(tt), a(aa);
	WordAlignedSet was(s, t, a, *model.SourceVocab(), *model.TargetVocab());
	model.WriteDocumentPredictions(was, docdist);
	int oovCount, wordCount;
	double logprob = model.LogLikelihood(was, wordCount, oovCount);
	double oovPenalty = -5;
	cout << "log-likelihood               = " << logprob << endl;
	cout << "word count                   = " << wordCount << endl;
	cout << "OOV count                    = " << oovCount << endl;
	cout << "avg logprob (skip OOV)       = " << logprob / (wordCount - oovCount) << endl;
	cout << "OOV penalty                  = " << oovPenalty << endl;
	cout << "avg logprob (OOVs penalized) = " << (logprob + oovCount * oovPenalty) / wordCount << endl;
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		cerr << "ERROR: need a command -- try train or test." << endl;
		return 1;
	}
	if (strcmp(argv[1], "test") == 0)
	{
		if (argc != 7)
		{
			cerr << "ERROR: need 5 args after train" << endl;
			cerr << "\t" << argv[0] << " model src tgt align docdist.txt" << endl;
			return 1;
		}
		try
		{
			Test(argv[2], argv[3], argv[4], argv[5], argv[6]);
		}
		catch (exception & e)
		{
			cerr << "ERROR: caught exception " << e.what() << endl;
			return 2;
		}
		return 0;
	}
	else if (strcmp(argv[1], "train") == 0)
	{
		if (argc != 9)
		{
			cerr << "ERROR: need 7 args after train" << endl;
			cerr << "\t" << argv[0] << "train src tgt align numTopics savedmodel.bin docdist.txt worddist.txt" << endl;
			return 1;
		}
		try
		{
			Train(argv[2], argv[3], argv[4], atoi(argv[5]), argv[6], argv[7], argv[8]);
		}
		catch (exception & e)
		{
			cerr << "ERROR: caught exception " << e.what() << endl;
			return 2;
		}
		return 0;
	}
	else if (strcmp(argv[1], "train_sgd") == 0)
	{
		if (argc != 9)
		{
			cerr << "ERROR: need 7 args after train" << endl;
			cerr << "\t" << argv[0] << "train src tgt align numTopics savedmodel.bin docdist.txt worddist.txt" << endl;
			return 1;
		}
		try
		{
			TrainSgd(argv[2], argv[3], argv[4], atoi(argv[5]), argv[6], argv[7], argv[8]);
		}
		catch (exception & e)
		{
			cerr << "ERROR: caught exception " << e.what() << endl;
			return 2;
		}
		return 0;
	}
	cerr << "ERROR: unknown command " << argv[1] << "; try train or test" << endl;
	return 1;
}

// vim:sw=4:ts=4:ai:cindent
