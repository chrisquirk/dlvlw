dlvlw : dlvlw.o stdafx.o
	g++ -o dlvlw dlvlw.o stdafx.o

dlvlw.o : dlvlw.cpp stdafx.h targetver.h
	g++ -O3 -c -Wno-deprecated dlvlw.cpp

stdafx.o : stdafx.cpp stdafx.h targetver.h
	g++ -O3 -c -Wno-deprecated stdafx.cpp

clean :
	rm dlvlw dlvlw.o stdafx.o
